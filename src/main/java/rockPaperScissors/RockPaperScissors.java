package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");


    public String chooseRandom(){
        Random rand = new Random();
        return rpsChoices.get(rand.nextInt(rpsChoices.size()));
    }
    
    public void run() {
        while (true) {
            System.out.printf("Let's play round %d\n",roundCounter);
            String humanChoice = userChoice();
            String computerChoice = chooseRandom();
            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";

            if (isWinner(humanChoice, computerChoice)){
                System.out.println(choiceString + " Human wins.");
                humanScore ++;
            }
            else if (isWinner(computerChoice, humanChoice)){
                System.out.println(choiceString + " Computer wins.");
                computerScore ++;
            }
            else {
                System.out.println(choiceString + " It's a tie.");
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            if (!continuePlaying()){

                break;

            }

        }
        System.out.println("Bye bye :)");

    }


    public Boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }

    public String userChoice (){
        while (true){
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(humanChoice)){
                return humanChoice;
            }
            else{
                System.out.printf("I do not understand %s. Could you try again?\n",humanChoice);
            }
        }
    }

    public Boolean continuePlaying(){
        while (true){
            String choice = readInput("Do you wish to continue playing? (y/n)?");
            roundCounter ++;
            return choice.equals("y");

        }
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
